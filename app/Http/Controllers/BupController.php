<?php

namespace App\Http\Controllers;

use App\Models\Bup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class BupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('search'))
        {
            $bup = Bup::where('nib', 'like', '%'.$request->search.'%')
                            ->orWhere('nama_pemilik', 'like', '%'.$request->search.'%')
                            ->paginate(10)->withQueryString();
        }else{
            $bup = Bup::paginate(10);
        }
        return view('admin.bup.index', compact('bup'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hak_akses == 'admin'){
            return view('admin.bup.create');
        }else{
            return redirect()->route('admin.bup.index')->with('warning', 'Anda tidak memiliki hak akses');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nib' => ['required', 'string', 'max:255', 'unique:bup'],
            'nama_pemilik' => ['required', 'string', 'max:255'],
            'nik' => ['required', 'string', 'min:16', 'max:255'],
            'no_rek' => ['required'],
            'ektp' => ['required'],
            'kk_pemilik' => ['required'],
            'ket_usaha' => ['required'],
            'buku_tabungan' => ['required'],
            'doc_pernyataan' => ['required'],
        ]);

        $ektp = $request->file('ektp')->storeAs(
            'pernyataan' , 'EKTP'.$request->nib, 'public'
        );

        $kk_pemilik = $request->file('kk_pemilik')->storeAs(
            'pernyataan' , 'KK'.$request->nib, 'public'
        );

        $ket_usaha = $request->file('ket_usaha')->storeAs(
            'pernyataan' , 'KU'.$request->nib, 'public'
        );

        $buku_tabungan = $request->file('buku_tabungan')->storeAs(
            'pernyataan' , 'BT'.$request->nib, 'public'
        );

        $doc_pernyataan = $request->file('doc_pernyataan')->storeAs(
            'pernyataan' , 'P'.$request->nib, 'public'
        );

        $store = $request->except(['ektp', 'kk_pemilik', 'ket_usaha', 'buku_tabungan', 'doc_pernyataan']);

        $store['ektp'] = $ektp;
        $store['kk_pemilik'] = $kk_pemilik;
        $store['ket_usaha'] = $ket_usaha;
        $store['buku_tabungan'] = $buku_tabungan;
        $store['doc_pernyataan'] = $doc_pernyataan;

        $bup = Bup::create($store);
        
        return redirect()->route('admin.bup.show', $bup)->with('success', 'Berhasil menambah BUP <strong>'.$bup->nib.'</strong>.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bup  $bup
     * @return \Illuminate\Http\Response
     */
    public function show(Bup $bup)
    {
        return view('admin.bup.show', compact('bup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bup  $bup
     * @return \Illuminate\Http\Response
     */
    public function edit(Bup $bup)
    {
        if(Auth::user()->hak_akses == 'admin'){
            return view('admin.bup.edit', compact('bup'));
        }else{
            return redirect()->route('admin.bup.index')->with('warning', 'Anda tidak memiliki hak akses');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bup  $bup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bup $bup)
    {
        $request->validate([
            'nib' => ['required', 'string', 'max:255', 'unique:bup,nib,'.$bup->id_bup.',id_bup'],
            'nama_pemilik' => ['required', 'string', 'max:255'],
            'nik' => ['required', 'string', 'min:16', 'max:255'],
            'no_rek' => ['required'],
        ]);
        
        $except = [];

        if($request->has('ektp')){
            Storage::disk('public')->delete($bup->ektp);
            $ektp = $request->file('ektp')->storeAs(
                'pernyataan' , 'EKTP'.$request->nib, 'public'
            );
            $except[] = 'ektp';
        }else{
            $ektp = false;
        }

        if($request->has('kk_pemilik')){
            Storage::disk('public')->delete($bup->kk_pemilik);
            $kk_pemilik = $request->file('kk_pemilik')->storeAs(
                'pernyataan' , 'KK'.$request->nib, 'public'
            );
            $except[] = 'kk_pemilik';
        }else{
            $kk_pemilik = false;
        }

        if($request->has('ket_usaha')){
            Storage::disk('public')->delete($bup->ket_usaha);
            $ket_usaha = $request->file('ket_usaha')->storeAs(
                'pernyataan' , 'KU'.$request->nib, 'public'
            );
            $except[] = 'ket_usaha';
        }else{
            $ket_usaha = false;
        }

        if($request->has('buku_tabungan')){
            Storage::disk('public')->delete($bup->buku_tabungan);
            $buku_tabungan = $request->file('buku_tabungan')->storeAs(
                'pernyataan' , 'BT'.$request->nib, 'public'
            );
            $except[] = 'buku_tabungan';
        }else{
            $buku_tabungan = false;
        }

        if($request->has('doc_pernyataan')){
            Storage::disk('public')->delete($bup->doc_pernyataan);
            $doc_pernyataan = $request->file('doc_pernyataan')->storeAs(
                'pernyataan' , 'P'.$request->nib, 'public'
            );
            $except[] = 'doc_pernyataan';
        }else{
            $doc_pernyataan = false;
        }

        if(count($except) > 0)
        {
            $update = $request->except($except);
            if($ektp){
                $update['ektp'] = $ektp;
            }
            if($kk_pemilik){
                $update['kk_pemilik'] = $kk_pemilik;
            }
            if($ket_usaha){
                $update['ket_usaha'] = $ket_usaha;
            }
            if($buku_tabungan){
                $update['buku_tabungan'] = $buku_tabungan;
            }
            if($doc_pernyataan){
                $update['doc_pernyataan'] = $doc_pernyataan;
            }
        }else{
            $update = $request->all();
        }

        $bup->update($update);
        
        return redirect()->route('admin.bup.show', $bup)->with('success', 'Berhasil mengubah BUP <strong>'.$bup->nib.'</strong>.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bup  $bup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bup $bup)
    {
        if(Auth::user()->hak_akses == 'admin'){
            $name = $bup->nib;
            Storage::disk('public')->delete($bup->ektp);
            Storage::disk('public')->delete($bup->kk_pemilik);
            Storage::disk('public')->delete($bup->buku_tabungan);
            Storage::disk('public')->delete($bup->ket_usaha);
            Storage::disk('public')->delete($bup->doc_pernyataan);
            $bup->delete();
    
            return redirect()->route('admin.bup.index')->with('success', 'Berhasil menghapus BUP <strong>'.$name.'</strong>.');
        }else{
            return redirect()->route('admin.bup.index')->with('warning', 'Anda tidak memiliki hak akses');
        }
    }

    /**
     * Downloading Doc EKTP
     * 
     * @param  \App\Models\Bup  $bup
     * @return \Illuminate\Http\Response
     */
    public function downloadEKTP(Bup $bup)
    {
        $file= public_path(). Storage::url($bup->ektp);

        $headers = array(
                'Content-Type: application/pdf',
                );

        return response()->download($file, 'EKTP_'.$bup->nib.'.pdf', $headers);
    }

    /**
     * Downloading Doc KK_Pemilik
     * 
     * @param  \App\Models\Bup  $bup
     * @return \Illuminate\Http\Response
     */
    public function downloadKK_Pemilik(Bup $bup)
    {
        $file= public_path(). Storage::url($bup->kk_pemilik);

        $headers = array(
                'Content-Type: application/pdf',
                );

        return response()->download($file, 'KK_Pemilik_'.$bup->nib.'.pdf', $headers);
    }

    /**
     * Downloading Doc Buku_Tabungan
     * 
     * @param  \App\Models\Bup  $bup
     * @return \Illuminate\Http\Response
     */
    public function downloadBuku_Tabungan(Bup $bup)
    {
        $file= public_path(). Storage::url($bup->buku_tabungan);

        $headers = array(
                'Content-Type: application/pdf',
                );

        return response()->download($file, 'Buku_Tabungan_'.$bup->nib.'.pdf', $headers);
    }

    /**
     * Downloading Doc Ket_Usaha
     * 
     * @param  \App\Models\Bup  $bup
     * @return \Illuminate\Http\Response
     */
    public function downloadKet_Usaha(Bup $bup)
    {
        $file= public_path(). Storage::url($bup->ket_usaha);

        $headers = array(
                'Content-Type: application/pdf',
                );

        return response()->download($file, 'Ket_Usaha_'.$bup->nib.'.pdf', $headers);
    }

    /**
     * Downloading Doc Pernyataan
     * 
     * @param  \App\Models\Bup  $bup
     * @return \Illuminate\Http\Response
     */
    public function downloadPernyataan(Bup $bup)
    {
        $file= public_path(). Storage::url($bup->doc_pernyataan);

        $headers = array(
                'Content-Type: application/pdf',
                );

        return response()->download($file, 'SuratPernyataan_'.$bup->nib.'.pdf', $headers);
    }
}
