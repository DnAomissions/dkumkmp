<?php

namespace App\Http\Controllers;

use App\Models\ProdukUmkm;
use App\Models\ProdukUnggulan;
use Illuminate\Http\Request;

class ProdukUnggulanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('search'))
        {
            $search = $request->search;
            $produkUnggulan = ProdukUnggulan::where('bpom', 'like', '%'.$request->search.'%')
                                ->orWhereHas('produkUmkm', function ($query) use ($search){
                                    $query->where('nama_produk', 'like', '%'.$search.'%');
                                })
                                ->paginate(10)->withQueryString();
        }else{
            $produkUnggulan = ProdukUnggulan::paginate(10);
        }
        return view('admin.produkUnggulan.index', compact('produkUnggulan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $produkUmkm = ProdukUmkm::all();
        return view('admin.produkUnggulan.create', compact('produkUmkm'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bpom' => ['required', 'string', 'max:255', 'unique:produk_unggulan'],
            'standart_kemasan' => ['required', 'string', 'max:255'],
        ]);

        $produkUmkm = ProdukUmkm::where('bpom', $request->bpom)->first();

        $request->request->add([
            'nib' => $produkUmkm->nib,
            'nama_usaha' => $produkUmkm->daftarUmkm->nama_usaha,
            'alamat_usaha' => $produkUmkm->daftarUmkm->alamat_usaha,
            'telp_usaha' => $produkUmkm->daftarUmkm->telp_usaha,
            'nama_produk' => $produkUmkm->nama_produk,
            'gambar_produk' => $produkUmkm->gambar_produk,
            'kategori' => $produkUmkm->kategori,
            'sertifikasi_halal' => $produkUmkm->sertifikasi_halal,
            'deskripsi_produk' => $produkUmkm->deskripsi_produk,
        ]);
        
        $produkUnggulan = ProdukUnggulan::create($request->all());
        
        return redirect()->route('admin.produkUnggulan.show', $produkUnggulan)->with('success', 'Berhasil menambah Produk Unggulan <strong>'.$produkUnggulan->nama_produk.'</strong>.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models ProdukUnggulan  $produkUnggulan
     * @return \Illuminate\Http\Response
     */
    public function show(ProdukUnggulan $produkUnggulan)
    {
        return view('admin.produkUnggulan.show', compact('produkUnggulan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models ProdukUnggulan  $produkUnggulan
     * @return \Illuminate\Http\Response
     */
    public function edit(ProdukUnggulan $produkUnggulan)
    {
        $produkUmkm = ProdukUmkm::all();
        return view('admin.produkUnggulan.edit', compact('produkUnggulan', 'produkUmkm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models ProdukUnggulan  $produkUnggulan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProdukUnggulan $produkUnggulan)
    {
        $request->validate([
            'bpom' => ['required', 'string', 'max:255', 'unique:produk_unggulan,bpom,'.$produkUnggulan->id_unggulan.',id_unggulan'],
            'standart_kemasan' => ['required', 'string', 'max:255'],
        ]);

        $produkUmkm = ProdukUmkm::where('bpom', $request->bpom)->first();

        $request->request->add([
            'nib' => $produkUmkm->nib,
            'nama_usaha' => $produkUmkm->daftarUmkm->nama_usaha,
            'alamat_usaha' => $produkUmkm->daftarUmkm->alamat_usaha,
            'telp_usaha' => $produkUmkm->daftarUmkm->telp_usaha,
            'nama_produk' => $produkUmkm->nama_produk,
            'gambar_produk' => $produkUmkm->gambar_produk,
            'kategori' => $produkUmkm->kategori,
            'sertifikasi_halal' => $produkUmkm->sertifikasi_halal,
            'deskripsi_produk' => $produkUmkm->deskripsi_produk,
        ]);

        $produkUnggulan->update($request->all());
        
        return redirect()->route('admin.produkUnggulan.show', $produkUnggulan)->with('success', 'Berhasil mengubah Produk Unggulan <strong>'.$produkUnggulan->nama_produk.'</strong>.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models ProdukUnggulan  $produkUnggulan
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProdukUnggulan $produkUnggulan)
    {
        $name = $produkUnggulan->nama_produk;
        $produkUnggulan->delete();

        return redirect()->route('admin.produkUnggulan.index')->with('success', 'Berhasil menghapus Produk Unggulan <strong>'.$name.'</strong>.');
    }
}
