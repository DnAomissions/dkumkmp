<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdukUmkm extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'produk_umkm';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the daftarUmkm for the produk.
     */
    public function daftarUmkm()
    {
        return $this->belongsTo(DaftarUmkm::class, 'nib', 'nib');
    }
}
