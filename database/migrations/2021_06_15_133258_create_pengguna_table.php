<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenggunaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengguna', function (Blueprint $table) {
            $table->id();
            $table->string('nik_pengguna')->unique();
            $table->string('nama_pengguna');
            $table->string('password');
            $table->enum('instansi', ['DKUMKMP', 'DPMPT']);
            $table->string('no_telp')->nullable();
            $table->text('alamat')->nullable();
            $table->enum('hak_akses', ['staf_dpmpt', 'staf_dkumkmp', 'admin']);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengguna');
    }
}
