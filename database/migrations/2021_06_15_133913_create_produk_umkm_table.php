<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdukUmkmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk_umkm', function (Blueprint $table) {
            $table->id();
            $table->string('bpom')->unique();
            $table->string('nib');
            $table->string('nama_usaha');
            $table->text('alamat_usaha')->nullable();
            $table->string('telp_usaha')->nullable();
            $table->string('nama_produk');
            $table->string('gambar_produk');
            $table->enum('kategori', ['Fashion', 'Teknologi', 'Kuliner', 'Cinderamata', 'Kosmetik', 'Agro Bisnis', 'Otomotif']);
            $table->enum('sertifikasi_halal', ['Sudah Tersertifikasi', 'Belum Tersertifikasi']);
            $table->text('deskripsi_produk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk_umkm');
    }
}
