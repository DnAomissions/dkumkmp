<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalPameranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_pameran', function (Blueprint $table) {
            $table->id('id_pameran');
            $table->string('nama_pameran');
            $table->string('poster_pameran');
            $table->date('tgl_pameran');
            $table->text('lokasi_pameran');
            $table->text('deskripsi_pameran')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_pameran');
    }
}
