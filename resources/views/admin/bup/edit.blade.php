@php
    $title = 'Pengajuan BUP';
    $page = 'Pengajuan BUP';
    $pageTitle = 'Pengajuan BUP';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Pengajuan BUP',
            'link' => route('admin.bup.index'),
        ],
        [
            'name' => $bup->nib.' ('.$bup->nama_pemilik.')',
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.bup.show', $bup) }}" class="btn btn-secondary mr-1">Back</a>
    <button type="button" id="button-submit" class="btn btn-outline-success mr-2">Update</button>
</div>
@endpush

@section('content')
    @include('admin.bup.view.form', ['bup' => $bup, 'form_open' => true])
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $('#button-submit').click(function () {
                $('#form-bup').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@endpush