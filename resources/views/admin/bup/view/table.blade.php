
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>NIB</th>
                <th>Nama Pemilik</th>
                <th>NIK</th>
                <th>IUMK</th>
                <th>Alamat Usaha</th>
                <th>Telp. Pemilik</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        @if (count($data) <= 0)
            <tr>
                <td colspan="11" class="text-center text-muted">Belum ada Pengajuan BUP!</td>
            </tr>
        @endif
        @foreach ($data as $row)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><a href="{{ route('admin.bup.show', $row) }}">{{ $row->nib }}</a></td>
                <td>{{ $row->nama_pemilik }}</td>
                <td>{{ $row->nik }}</td>
                <td>{{ $row->iumk }}</td>
                <td>{{ $row->alamat_usaha }}</td>
                <td>{{ $row->telp_pemilik }}</td>
                <td>
                    <form action="{{ route('admin.bup.destroy', $row) }}" id="form-delete" onsubmit="return confirm('Apakah anda yakin untuk menghapus {{ $row->nib }}?');" method="POST">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>    
@include('components.pagination', ['data' => $data])