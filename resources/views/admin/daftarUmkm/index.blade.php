@php
    $page = 'Daftar UMKM';
@endphp

@extends('layouts.dashboard',[
    'title' => 'Daftar UMKM',
    'page' => $page,
    'pageTitle' => 'Daftar UMKM',
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => $page,
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<form action="{{ route('admin.daftarUmkm.index') }}" method="get" class="input-group mr-3">
    <input type="text" name="search" id="search" value="{{ $_GET['search'] ?? '' }}" placeholder="Search..." class="form-control"/>
    <div class="input-group-append">
        <button type="submit" class="btn btn-info text-white"><i class="fas fa-search"></i></button>
    </div>
</form>
<div class="btn-group me-2">
    @if (Auth::user()->hak_akses != 'staf_dkumkmp')
        <a href="{{ route('admin.daftarUmkm.create') }}" class="btn btn-outline-success">Create</a>
    @endif
</div>
@endpush

@section('content')
    @include('admin.daftarUmkm.view.table', [
        'data' => $daftarUmkm
    ])
@endsection