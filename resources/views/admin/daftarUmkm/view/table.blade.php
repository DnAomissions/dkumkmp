
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>NIB</th>
                <th>Nama Pemilik</th>
                <th>NIK</th>
                <th>IUMK</th>
                <th>Tanggal Terbit</th>
                <th>Kekayaan</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        @if (count($data) <= 0)
            <tr>
                <td colspan="8" class="text-center text-muted">Belum ada Daftar UMKM!</td>
            </tr>
        @endif
        @foreach ($data as $row)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><a href="{{ route('admin.daftarUmkm.show', $row) }}">{{ $row->nib }}</a></td>
                <td>{{ $row->nama_pemilik }}</td>
                <td>{{ $row->nik }}</td>
                <td>{{ $row->iumk }}</td>
                <td>{{ $row->tgl_terbit }}</td>
                <td>{{ $row->kekayaan }}</td>
                <td>
                    @if (Auth::user()->hak_akses == 'admin')
                        <form action="{{ route('admin.daftarUmkm.destroy', $row) }}" id="form-delete" onsubmit="return confirm('Apakah anda yakin untuk menghapus {{ $row->nib }}?');" method="POST">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                        </form>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>    
@include('components.pagination', ['data' => $data])