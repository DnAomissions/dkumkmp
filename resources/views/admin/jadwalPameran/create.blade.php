@php
    $title = 'Pameran';
    $page = 'Pameran';
    $pageTitle = 'Pameran';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Pameran',
            'link' => route('admin.jadwalPameran.index'),
        ],
        [
            'name' => 'New',
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.jadwalPameran.index') }}" class="btn btn-secondary mr-1">Discard</a>
    @if (Auth::user()->hak_akses != 'staf_dpmpt')
        <button type="button" id="button-submit" class="btn btn-outline-success mr-2">Save</button>
    @endif
</div>
@endpush

@section('content')
    @include('admin.jadwalPameran.view.form', ['jadwalPameran' => false, 'form_open' => true])
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $('#button-submit').click(function () {
                $('#form-jadwalPameran').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@endpush