@php
    $title = 'Pameran';
    $page = 'Pameran';
    $pageTitle = 'Pameran';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Pameran',
            'link' => route('admin.jadwalPameran.index'),
        ],
        [
            'name' => $jadwalPameran->nama_pameran.' ('.$jadwalPameran->tgl_pameran.')',
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.jadwalPameran.index') }}" class="btn btn-secondary mr-1">Back</a>
    @if (Auth::user()->hak_akses != 'staf_dpmpt')
        <a href="{{ route('admin.jadwalPameran.edit', $jadwalPameran) }}" class="btn btn-primary">Edit</a>
    @endif
</div>
@endpush

@section('content')
    @include('admin.jadwalPameran.view.form', ['jadwalPameran' => $jadwalPameran, 'form_open' => false])
@endsection