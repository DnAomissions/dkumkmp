@php
    $page = 'Daftar Pengguna';
@endphp

@extends('layouts.dashboard',[
    'title' => 'Daftar Pengguna',
    'page' => $page,
    'pageTitle' => 'Daftar Pengguna',
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => $page,
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<form action="{{ route('admin.pengguna.index') }}" method="get" class="input-group mr-3">
    <input type="text" name="search" id="search" value="{{ $_GET['search'] ?? '' }}" placeholder="Search..." class="form-control"/>
    <div class="input-group-append">
        <button type="submit" class="btn btn-info text-white"><i class="fas fa-search"></i></button>
    </div>
</form>
<div class="btn-group me-2">
    @if (Auth::user()->hak_akses == 'admin')
        <a href="{{ route('admin.pengguna.create') }}" class="btn btn-outline-success">Create</a>
    @endif
</div>
@endpush

@section('content')
    @include('admin.pengguna.view.table', [
        'data' => $pengguna
    ])
@endsection