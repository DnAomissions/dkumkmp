@php
    $title = 'Daftar Pengguna';
    $page = 'Daftar Pengguna';
    $pageTitle = 'Daftar Pengguna';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Daftar Pengguna',
            'link' => route('admin.pengguna.index'),
        ],
        [
            'name' => $pengguna->nik_pengguna.' ('.$pengguna->nama_pengguna.')',
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.pengguna.index') }}" class="btn btn-secondary mr-1">Back</a>
    
    @if (Auth::user()->hak_akses == 'admin')
        <a href="{{ route('admin.pengguna.edit', $pengguna) }}" class="btn btn-primary">Edit</a>
    @endif
</div>
@endpush

@section('content')
    @include('admin.pengguna.view.form', ['pengguna' => $pengguna, 'form_open' => false])
@endsection