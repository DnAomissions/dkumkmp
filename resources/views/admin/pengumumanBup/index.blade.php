@php
    $page = 'Pengumuman BUP';
@endphp

@extends('layouts.dashboard',[
    'title' => 'Pengumuman BUP',
    'page' => $page,
    'pageTitle' => 'Pengumuman BUP',
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => $page,
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<form action="{{ route('admin.pengumumanBup.index') }}" method="get" class="input-group mr-3">
    <input type="text" name="search" id="search" value="{{ $_GET['search'] ?? '' }}" placeholder="Search..." class="form-control"/>
    <div class="input-group-append">
        <button type="submit" class="btn btn-info text-white"><i class="fas fa-search"></i></button>
    </div>
</form>
<div class="btn-group me-2">
    <a href="{{ route('admin.pengumumanBup.create') }}" class="btn btn-outline-success">Create</a>
</div>
@endpush

@section('content')
    @include('admin.pengumumanBup.view.kanban', [
        'data' => $pengumumanBup
    ])
@endsection