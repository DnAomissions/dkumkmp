@php
    $title = 'Pengumuman BUP';
    $page = 'Pengumuman BUP';
    $pageTitle = 'Pengumuman BUP';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Pengumuman BUP',
            'link' => route('admin.pengumumanBup.index'),
        ],
        [
            'name' => $pengumumanBup->judul_pengumuman,
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.pengumumanBup.index') }}" class="btn btn-secondary mr-1">Back</a>
    <a href="{{ route('admin.pengumumanBup.edit', $pengumumanBup) }}" class="btn btn-primary">Edit</a>
</div>
@endpush

@section('content')
    @include('admin.pengumumanBup.view.form', ['pengumumanBup' => $pengumumanBup, 'form_open' => false])
@endsection