
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Pengumuman BUP</th>
                <th>Poster</th>
                <th>Tanggal Pengumuman BUP</th>
                <th>Lokasi</th>
                <th>Deskripsi</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        @if (count($data) <= 0)
            <tr>
                <td colspan="7" class="text-center text-muted">Belum ada Pengumuman BUP!</td>
            </tr>
        @endif
        @foreach ($data as $row)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><a href="{{ route('admin.pengumumanBup.show', $row) }}">{{ $row->judul_pengumuman }}</a></td>
                <td>{{ $row->poster_pameran }}</td>
                <td>{{ $row->tgl_pameran }}</td>
                <td>{{ $row->lokasi_pameran }}</td>
                <td>{{ $row->deskripsi_pameran }}</td>
                <td>
                    <form action="{{ route('admin.pengumumanBup.destroy', $row) }}" id="form-delete" onsubmit="return confirm('Apakah anda yakin untuk menghapus {{ $row->judul_pengumuman }}?');" method="POST">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@include('components.pagination', ['data' => $data])