@php
    $title = 'Produk UMKM';
    $page = 'Produk UMKM';
    $pageTitle = 'Produk UMKM';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Produk UMKM',
            'link' => route('admin.produkUmkm.index'),
        ],
        [
            'name' => $produkUmkm->nama_produk,
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.produkUmkm.show', $produkUmkm) }}" class="btn btn-secondary mr-1">Back</a>
    @if (Auth::user()->hak_akses != 'staf_dpmpt')
        <button type="button" id="button-submit" class="btn btn-outline-success mr-2">Update</button>
    @endif
</div>
@endpush

@section('content')
    @include('admin.produkUmkm.view.form', ['produkUmkm' => $produkUmkm, 'form_open' => true])
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $('#button-submit').click(function () {
                $('#form-produkUmkm').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@endpush