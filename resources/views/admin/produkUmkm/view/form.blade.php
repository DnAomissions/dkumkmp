<form id="form-produkUmkm" action="{{ ($produkUmkm) ? route('admin.produkUmkm.update', $produkUmkm) : route('admin.produkUmkm.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="_method" value="{{ ($produkUmkm) ? 'PUT' : 'POST' }}" />
    <div class="form-group row">
        <label for="bpom" class="col-md-4 col-form-label text-md-right">{{ __('BPOM') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="bpom" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('bpom') is-invalid @enderror" name="bpom" value="{{ (old('bpom')) ? old('bpom') : $produkUmkm->bpom ?? '' }}" autocomplete="bpom" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('bpom')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="nib" class="col-md-4 col-form-label text-md-right">{{ __('NIB') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            @if ($form_open)
                <select id="nib" class="form-control select2 @error('nib') is-invalid @enderror" name="nib" required {{ ($form_open) ? '' : 'disabled' }}>
                    <option value="" disabled selected>-- NIB --</option>
                @foreach ($daftarUmkm as $row)
                    <option value="{{ $row->nib }}" {{ (((old('nib')) ? old('nib') : $produkUmkm->nib ?? '') == $row->nib) ? 'selected' : '' }}>{{ $row->nib.' ('.$row->nama_usaha.')'}}</option>
                @endforeach
                </select>
            @else
                <a href="{{ route('admin.daftarUmkm.show', $produkUmkm->daftarUmkm) }}" class="form-control-plaintext text-primary">{{ $produkUmkm->daftarUmkm->nib.' ('.$produkUmkm->daftarUmkm->nama_usaha.')' }}</a>
            @endif

            @error('nib')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="nama_produk" class="col-md-4 col-form-label text-md-right">{{ __('Nama Produk') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="nama_produk" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('nama_produk') is-invalid @enderror" name="nama_produk" value="{{ (old('nama_produk')) ? old('nama_produk') : $produkUmkm->nama_produk ?? '' }}" autocomplete="nama_produk" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('nama_produk')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="kategori" class="col-md-4 col-form-label text-md-right">{{ __('Kategori') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <select id="kategori" class="form-control @error('kategori') is-invalid @enderror" name="kategori" required {{ ($form_open) ? '' : 'disabled' }}>
                <option value="" disabled selected>-- Kategori --</option>
                <option value="Fashion" {{ (((old('kategori')) ? old('kategori') : $produkUmkm->kategori ?? '') == "Fashion") ? 'selected' : '' }}>Fashion</option>
                <option value="Teknologi" {{ (((old('kategori')) ? old('kategori') : $produkUmkm->kategori ?? '') == "Teknologi") ? 'selected' : '' }}>Teknologi</option>
                <option value="Kuliner" {{ (((old('kategori')) ? old('kategori') : $produkUmkm->kategori ?? '') == "Kuliner") ? 'selected' : '' }}>Kuliner</option>
                <option value="Cinderamata" {{ (((old('kategori')) ? old('kategori') : $produkUmkm->kategori ?? '') == "Cinderamata") ? 'selected' : '' }}>Cinderamata</option>
                <option value="Kosmetik" {{ (((old('kategori')) ? old('kategori') : $produkUmkm->kategori ?? '') == "Kosmetik") ? 'selected' : '' }}>Kosmetik</option>
                <option value="Agro Bisnis" {{ (((old('kategori')) ? old('kategori') : $produkUmkm->kategori ?? '') == "Agro Bisnis") ? 'selected' : '' }}>Agro Bisnis</option>
                <option value="Otomotif" {{ (((old('kategori')) ? old('kategori') : $produkUmkm->kategori ?? '') == "Otomotif") ? 'selected' : '' }}>Otomotif</option>
            </select>

            @error('kategori')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="sertifikasi_halal" class="col-md-4 col-form-label text-md-right">{{ __('Sertifikasi Halal') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <select id="sertifikasi_halal" class="form-control @error('sertifikasi_halal') is-invalid @enderror" name="sertifikasi_halal" required {{ ($form_open) ? '' : 'disabled' }}>
                <option value="" disabled selected>-- Sertifikasi Halal --</option>
                <option value="Sudah Tersertifikasi" {{ (((old('sertifikasi_halal')) ? old('sertifikasi_halal') : $produkUmkm->sertifikasi_halal ?? '') == "Sudah Tersertifikasi") ? 'selected' : '' }}>Sudah Tersertifikasi</option>
                <option value="Belum Tersertifikasi" {{ (((old('sertifikasi_halal')) ? old('sertifikasi_halal') : $produkUmkm->sertifikasi_halal ?? '') == "Belum Tersertifikasi") ? 'selected' : '' }}>Belum Tersertifikasi</option>
            </select>

            @error('sertifikasi_halal')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="deskripsi_produk" class="col-md-4 col-form-label text-md-right">{{ __('Deskripsi') }}</label>

        <div class="col-md-6">
            <textarea id="deskripsi_produk" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('deskripsi_produk') is-invalid @enderror" name="deskripsi_produk" autocomplete="deskripsi_produk" {{ ($form_open) ? '' : 'readonly' }}>{{ (old('deskripsi_produk')) ? old('deskripsi_produk') : $produkUmkm->deskripsi_produk ?? '' }}</textarea>

            @error('deskripsi_produk')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="gambar_produk" class="col-md-4 col-form-label text-md-right">{{ __('Gambar Produk') }}{!! ($produkUmkm) ? '' : '&nbsp;<span class="text-danger">*</span>' !!}</label>

        <div class="col-md-6">
            @if ($form_open)
            <div class="custom-file">
                <input id="gambar_produk" type="file" accept="image/*" class="{{ ($form_open) ? 'custom-file-input' : 'form-control-plaintext' }} @error('gambar_produk') is-invalid @enderror" name="gambar_produk" value="{{ (old('gambar_produk')) ? old('gambar_produk') : $produkUmkm->gambar_produk ?? '' }}" autocomplete="gambar_produk" {{ ($produkUmkm) ? '' : 'required' }} {{ ($form_open) ? 'autofocus' : 'readonly' }}>
                <label class="custom-file-label" for="gambar_produk">Choose file</label>
            </div>
            @endif
            <img id="previewImg" src="{{ ($produkUmkm) ? asset('storage/'.$produkUmkm->gambar_produk) : '#' }}" class="mt-3 col-12"/>
            @error('gambar_produk')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    
    <button type="submit" style="display:none">SUBMIT</button>
</form>

@if ($form_open)
    <script>
        $(document).ready(function(){
            $('#gambar_produk').on('change keyup focus', function(e){
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#previewImg').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>
@endif