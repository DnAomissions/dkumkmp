@php
    $title = 'Produk Unggulan';
    $page = 'Produk Unggulan';
    $pageTitle = 'Produk Unggulan';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Produk Unggulan',
            'link' => route('admin.produkUnggulan.index'),
        ],
        [
            'name' => 'New',
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.produkUnggulan.index') }}" class="btn btn-secondary mr-1">Discard</a>
    @if (Auth::user()->hak_akses != 'staf_dpmpt')
        <button type="button" id="button-submit" class="btn btn-outline-success mr-2">Save</button>
    @endif
</div>
@endpush

@section('content')
    @include('admin.produkUnggulan.view.form', ['produkUnggulan' => false, 'form_open' => true])
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $('#button-submit').click(function () {
                $('#form-produkUnggulan').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@endpush