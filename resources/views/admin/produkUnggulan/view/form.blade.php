<form id="form-produkUnggulan" action="{{ ($produkUnggulan) ? route('admin.produkUnggulan.update', $produkUnggulan) : route('admin.produkUnggulan.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="_method" value="{{ ($produkUnggulan) ? 'PUT' : 'POST' }}" />
    <div class="form-group row">
        <label for="bpom" class="col-md-4 col-form-label text-md-right">{{ __('BPOM') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            @if ($form_open)
                <select id="bpom" class="form-control select2 @error('bpom') is-invalid @enderror" name="bpom" required {{ ($form_open) ? '' : 'disabled' }}>
                    <option value="" disabled selected>-- BPOM --</option>
                @foreach ($produkUmkm as $row)
                    <option value="{{ $row->bpom }}" {{ (((old('bpom')) ? old('bpom') : $produkUnggulan->bpom ?? '') == $row->bpom) ? 'selected' : '' }}>{{ $row->bpom.' ('.$row->nama_produk.')'}}</option>
                @endforeach
                </select>
            @else
                <a href="{{ route('admin.produkUmkm.show', $produkUnggulan->produkUmkm) }}" class="form-control-plaintext text-primary">{{ $produkUnggulan->produkUmkm->bpom.' ('.$produkUnggulan->produkUmkm->nama_produk.')' }}</a>
            @endif

            @error('bpom')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="standart_kemasan" class="col-md-4 col-form-label text-md-right">{{ __('Standart Kemasan') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <select id="standart_kemasan" class="form-control @error('standart_kemasan') is-invalid @enderror" name="standart_kemasan" required {{ ($form_open) ? '' : 'disabled' }}>
                <option value="" disabled selected>-- Standart Kemasan --</option>
                <option value="Cukup" {{ (((old('standart_kemasan')) ? old('standart_kemasan') : $produkUnggulan->standart_kemasan ?? '') == "Cukup") ? 'selected' : '' }}>Cukup</option>
                <option value="Bagus" {{ (((old('standart_kemasan')) ? old('standart_kemasan') : $produkUnggulan->standart_kemasan ?? '') == "Bagus") ? 'selected' : '' }}>Bagus</option>
                <option value="Sangat Bagus" {{ (((old('standart_kemasan')) ? old('standart_kemasan') : $produkUnggulan->standart_kemasan ?? '') == "Sangat Bagus") ? 'selected' : '' }}>Sangat Bagus</option>
            </select>

            @error('standart_kemasan')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <button type="submit" style="display:none">SUBMIT</button>
</form>

@if ($form_open)
    <script>
        $(document).ready(function(){
            $('#gambar_produk').on('change keyup focus', function(e){
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#previewImg').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>
@endif