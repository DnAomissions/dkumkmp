
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Produk Unggulan</th>
                <th>Poster</th>
                <th>Tanggal Produk Unggulan</th>
                <th>Lokasi</th>
                <th>Deskripsi</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        @if (count($data) <= 0)
            <tr>
                <td colspan="7" class="text-center text-muted">Belum ada Produk Unggulan!</td>
            </tr>
        @endif
        @foreach ($data as $row)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><a href="{{ route('admin.produkUnggulan.show', $row) }}">{{ $row->nama_pameran }}</a></td>
                <td>{{ $row->poster_pameran }}</td>
                <td>{{ $row->tgl_pameran }}</td>
                <td>{{ $row->lokasi_pameran }}</td>
                <td>{{ $row->deskripsi_pameran }}</td>
                <td>
                @if (Auth::user()->hak_akses != 'staf_dpmpt')
                    <form action="{{ route('admin.produkUnggulan.destroy', $row) }}" id="form-delete" onsubmit="return confirm('Apakah anda yakin untuk menghapus {{ $row->nama_pameran }}?');" method="POST">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                    </form>
                @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@include('components.pagination', ['data' => $data])