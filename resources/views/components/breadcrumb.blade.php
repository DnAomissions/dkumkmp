<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
    @foreach ($breadcrumbItems as $breadcrumbItem)
        <li class="breadcrumb-item">
            @if($breadcrumbItem['link'] == 'javascript:void(0)' || $breadcrumbItem['link'] == '#')
                {!! $breadcrumbItem['name'] !!}
            @else
                <a href="{{ $breadcrumbItem['link'] }}">{!! $breadcrumbItem['name'] !!}</a>
            @endif
        </li>
    @endforeach
    </ol>
</nav>