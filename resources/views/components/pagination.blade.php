<div class="row">
    <div class="col-6">
        Showing {{ $data->firstItem() }} to {{ $data->lastItem() }} of {{ $data->total() }} Result
    </div>
    <div class="col-6">
        {{-- Pagination --}}
        <div class="btn-group float-right">
            @if ( $data->previousPageUrl() != null)
                <a href="{{ $data->previousPageUrl() }}" class="btn btn-outline-primary"><</a>
            @endif

            {{-- Start Number Page --}}
            @if ( $data->currentPage() != 1)
                <a href="{{ $data->url(1) }}" class="btn btn-outline-primary">1</a>
            @endif
            @if ( $data->currentPage()-2 > 2)
                <button disabled class="btn btn-outline-primary">...</button>
            @endif

            @if ( $data->url($data->currentPage()-2) != null && $data->currentPage()-2 > 1)
                <a href="{{ $data->url($data->currentPage()-2) }}" class="btn btn-outline-primary">{{ $data->currentPage()-2 }}</a>
            @endif
            @if ( $data->url($data->currentPage()-1) != null && $data->currentPage()-1 > 1)
                <a href="{{ $data->url($data->currentPage()-1) }}" class="btn btn-outline-primary">{{ $data->currentPage()-1 }}</a>
            @endif
            <button disabled class="btn btn-primary">{{ $data->currentPage() }}</button>
            @if ( $data->url($data->currentPage()+1) != null && $data->currentPage()+1 < $data->lastPage())
                <a href="{{ $data->url($data->currentPage()+1) }}" class="btn btn-outline-primary">{{ $data->currentPage()+1 }}</a>
            @endif
            @if ( $data->url($data->currentPage()+2) != null && $data->currentPage()+2 < $data->lastPage())
                <a href="{{ $data->url($data->currentPage()+2) }}" class="btn btn-outline-primary">{{ $data->currentPage()+2 }}</a>
            @endif

            @if ( $data->currentPage()+2 < $data->lastPage()-1)
                <button disabled class="btn btn-outline-primary">...</button>
            @endif
            @if ( $data->currentPage() != $data->lastPage())
                <a href="{{ $data->url($data->lastPage()) }}" class="btn btn-outline-primary">{{ $data->lastPage() }}</a>
            @endif
            {{-- End Number Page --}}

            @if ( $data->nextPageUrl() != null)
                <a href="{{ $data->nextPageUrl() }}" class="btn btn-outline-primary">></a>
            @endif
        </div>
    </div>
</div>