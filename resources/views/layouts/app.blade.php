@extends('layouts.core')

@section('app')
    @include('layouts.components.header', ['public' => true])

    <main class="py-4">
        @yield('content')
    </main>
@endsection