<nav class="navbar navbar-expand-sm navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="{{ url('/') }}">{!! $pageTitle ?? 'DKUMKMP'!!}</a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  @if (!$public)
    <span class="w-100 text-white px-3">
      DKUMKMP
      <br>
      Kota Balikpapan
    </span>
  @endif
  <div class="collapse navbar-collapse" id="sidebarMenu">
    @if ($public)
    <ul class="navbar-nav ml-2">
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Beranda') ? 'active' : '' }}" aria-current="page" href="{{ route('public.beranda') }}">
          Beranda
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Daftar UMKM') ? 'active' : '' }}" aria-current="page" href="{{ route('public.daftarUmkm') }}">
          Daftar UMKM
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Produk UMKM') ? 'active' : '' }}" aria-current="page" href="{{ route('public.produkUmkm') }}">
          Produk UMKM
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Produk Unggulan') ? 'active' : '' }}" aria-current="page" href="{{ route('public.produkUnggulan') }}">
          Produk Unggulan
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Jadwal Pameran') ? 'active' : '' }}" aria-current="page" href="{{ route('public.jadwalPameran') }}">
          Jadwal Pameran
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'BUP') ? 'active' : '' }}" aria-current="page" href="{{ route('public.bup') }}">
          BUP
        </a>
      </li>
    </ul>
    @endif
    <ul class="navbar-nav ml-auto">
      <!-- Authentication Links -->
      @if ($public)
        @auth
          <li class="nav-item">
            <a class="nav-link {{ ($page == 'Beranda Admin') ? 'active' : '' }}" aria-current="page" href="{{ route('admin.beranda') }}">
              Beranda Admin
            </a>
          </li>
        @endauth
      @endif
      @guest
          {{-- <li class="nav-item ml-2">
              <a class="nav-link {{ ($page == 'Login') ? 'active' : '' }}" href="{{ route('login') }}">{{ __('Sign In') }}</a>
          </li>
          @if (Route::has('register'))
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
              </li>
          @endif --}}
      @else
          <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  {{ Auth::user()->nama_pengguna }}
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                      @csrf
                  </form>
              </div>
          </li>
      @endguest
      
  </ul>
  </div>
</nav>