@php
    $hak_akses = [
        'admin' => 'Admin',
        'staf_dpmpt' => 'DPMPT',
        'staf_dkumkmp' => 'DKUMKMP'
    ];
@endphp 
<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
  <div class="position-sticky pt-3">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link" aria-current="page" href="{{ route('public.beranda') }}">
          <i class="fas fa-globe-americas"></i>
          Beranda Publik
        </a>
      </li>
      <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Menu {{ $hak_akses[Auth::user()->hak_akses] }}</span>
      </h6>
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Beranda') ? 'active' : '' }}" aria-current="page" href="{{ route('admin.beranda') }}">
          <i class="fas fa-home"></i>
          Beranda
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Daftar UMKM') ? 'active' : '' }}" aria-current="page" href="{{ route('admin.daftarUmkm.index') }}">
          <i class="fas fa-store"></i>
          Daftar UMKM
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Produk UMKM') ? 'active' : '' }}" aria-current="page" href="{{ route('admin.produkUmkm.index') }}">
          <i class="fas fa-boxes"></i>
          Produk UMKM
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Produk Unggulan') ? 'active' : '' }}" aria-current="page" href="{{ route('admin.produkUnggulan.index') }}">
          <i class="fas fa-star"></i>
          Produk Unggulan
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Pameran') ? 'active' : '' }}" aria-current="page" href="{{ route('admin.jadwalPameran.index') }}">
          <i class="fas fa-th-large"></i>
          Pameran
        </a>
      </li>
    @if (Auth::user()->hak_akses != 'staf_dpmpt')
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Pengajuan BUP') ? 'active' : '' }}" aria-current="page" href="{{ route('admin.bup.index') }}">
          <i class="fas fa-clipboard-list"></i>
          Pengajuan BUP
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Pengumuman BUP') ? 'active' : '' }}" aria-current="page" href={{ route('admin.pengumumanBup.index') }}>
          <i class="fas fa-clipboard-check"></i>
          Pengumuman BUP
        </a>
      </li>
    @endif
      
    @if (Auth::user()->hak_akses == 'admin')
      <li class="nav-item">
        <a class="nav-link {{ ($page == 'Daftar Pengguna') ? 'active' : '' }}" aria-current="page" href="{{ route('admin.pengguna.index') }}">
          <i class="fas fa-users"></i>
          Daftar Pengguna
        </a>
      </li>
    @endif
    </ul>
  </div>
</nav>