<form id="form-bup" action="{{ route('public.bup.store') }}"id="form-bup" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="_method" value="{{ ($bup) ? 'PUT' : 'POST' }}" />

    <div class="form-group row">
        <label for="nib" class="col-md-4 col-form-label text-md-right">{{ __('NIB') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="nib" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('nib') is-invalid @enderror" name="nib" value="{{ (old('nib')) ? old('nib') : '' }}" autocomplete="nib" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('nib')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="nama_pemilik" class="col-md-4 col-form-label text-md-right">{{ __('Nama Pemilik') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="nama_pemilik" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('nama_pemilik') is-invalid @enderror" name="nama_pemilik" value="{{ (old('nama_pemilik')) ? old('nama_pemilik') : '' }}" autocomplete="nama_pemilik" required {{ ($form_open) ? '' : 'readonly' }}>

            @error('nama_pemilik')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="nik" class="col-md-4 col-form-label text-md-right">{{ __('NIK') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="nik" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('nik') is-invalid @enderror" name="nik" value="{{ (old('nik')) ? old('nik') : '' }}" autocomplete="nik" required {{ ($form_open) ? '' : 'readonly' }}>

            @error('nik')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="kk_pemilik" class="col-md-4 col-form-label text-md-right">{{ __('KK Pemilik') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="kk_pemilik" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('kk_pemilik') is-invalid @enderror" name="kk_pemilik" value="{{ (old('kk_pemilik')) ? old('kk_pemilik') : $bup->kk_pemilik ?? '' }}" autocomplete="kk_pemilik" required {{ ($form_open) ? '' : 'readonly' }}>

            @error('kk_pemilik')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="buku_tabungan" class="col-md-4 col-form-label text-md-right">{{ __('Buku Tabungan') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="buku_tabungan" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('buku_tabungan') is-invalid @enderror" name="buku_tabungan" value="{{ (old('buku_tabungan')) ? old('buku_tabungan') : $bup->buku_tabungan ?? '' }}" autocomplete="buku_tabungan" required {{ ($form_open) ? '' : 'readonly' }}>

            @error('buku_tabungan')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="alamat_usaha" class="col-md-4 col-form-label text-md-right">{{ __('Alamat Usaha') }}</label>

        <div class="col-md-6">
            <textarea id="alamat_usaha" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('alamat_usaha') is-invalid @enderror" name="alamat_usaha" autocomplete="alamat_usaha" {{ ($form_open) ? '' : 'readonly' }}>{{ (old('alamat_usaha')) ? old('alamat_usaha') : '' }}</textarea>

            @error('alamat_usaha')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="telp_pemilik" class="col-md-4 col-form-label text-md-right">{{ __('Telepon Pemilik') }}</label>

        <div class="col-md-6">
            <input id="telp_pemilik" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('telp_pemilik') is-invalid @enderror" name="telp_pemilik" value="{{ (old('telp_pemilik')) ? old('telp_pemilik') : '' }}" autocomplete="telp_pemilik" {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('telp_pemilik')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="npwp" class="col-md-4 col-form-label text-md-right">{{ __('NPWP') }}</label>

        <div class="col-md-6">
            <input id="npwp" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('npwp') is-invalid @enderror" name="npwp" value="{{ (old('npwp')) ? old('npwp') : '' }}" autocomplete="npwp" {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('npwp')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="no_rek" class="col-md-4 col-form-label text-md-right">{{ __('No Rekening') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="no_rek" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('no_rek') is-invalid @enderror" name="no_rek" value="{{ (old('no_rek')) ? old('no_rek') : '' }}" autocomplete="no_rek" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('no_rek')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="ket_usaha" class="col-md-4 col-form-label text-md-right">{{ __('Keterangan Usaha') }}</label>

        <div class="col-md-6">
            <textarea id="ket_usaha" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('ket_usaha') is-invalid @enderror" name="ket_usaha" autocomplete="ket_usaha" {{ ($form_open) ? '' : 'readonly' }}>{{ (old('ket_usaha')) ? old('ket_usaha') : $bup->ket_usaha ?? '' }}</textarea>

            @error('ket_usaha')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="doc_pernyataan" class="col-md-4 col-form-label text-md-right">{{ __('Dokumen Pernyataan') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            @if ($form_open)
            <div class="custom-file">
                <input id="doc_pernyataan" type="file" accept=".pdf" class="{{ ($form_open) ? 'custom-file-input' : 'form-control-plaintext' }} @error('doc_pernyataan') is-invalid @enderror" name="doc_pernyataan" value="{{ (old('doc_pernyataan')) ? old('doc_pernyataan') : '' }}" autocomplete="doc_pernyataan" {{ ($bup) ? '' : 'required' }} {{ ($form_open) ? 'autofocus' : 'readonly' }}>
                <label class="custom-file-label" for="doc_pernyataan">Choose file</label>
            </div>
            @endif
            @error('doc_pernyataan')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    
    <button type="submit" style="display:none">SUBMIT</button>
</form>