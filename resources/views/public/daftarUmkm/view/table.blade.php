<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama UMKM</th>
                <th>Sektor</th>
                <th>Kegiatan Usaha</th>
                <th>Alamat Usaha</th>
            </tr>
        </thead>
        <tbody>
        @if (count($data) <= 0)
            <tr>
                <td colspan="5" class="text-center text-muted">Belum ada Daftar UMKM!</td>
            </tr>
        @endif
        @foreach ($data as $row)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $row->nama_usaha }}</td>
                <td>{{ $row->sektor }}</td>
                <td>{{ $row->kegiatan_usaha }}</td>
                <td>{{ $row->alamat_usaha }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>    
@include('components.pagination', ['data' => $data])