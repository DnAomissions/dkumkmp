<div class="row mt-2 mb-2">
@if (count($data) <= 0)
    <h5 class="col-md-12 text-center text-muted">Belum ada Produk Unggulan!</h5>
@endif
@foreach ($data as $row)
        <div class="col-md-3 col-sm-6 mb-3">
            <div class="card">
                <div class="card-body row">
                    <div class="col-12 m-0 mt-1 p-0">
                        <img class="col-12 m-0" style="height:125px" src="{{ asset('storage/'.$row->produkUmkm->gambar_produk) }}"/>
                    </div>
                    <div class="col-12 pt-1 mt-1" style="height:190px">
                        <h5 class="card-title">{{ $row->produkUmkm->nama_produk }} <span class="text-muted small">({{ $row->produkUmkm->daftarUmkm->nama_usaha}})</span></h5>
                        <span class="card-subtitle">{!! $row->bpom !!}</span> | 
                        <span class="badge badge-primary text-wrap">{{ $row->kategori }}</span>
                        <p>{!! $row->produkUmkm->daftarUmkm->telp_pemilik.' - '.$row->produkUmkm->daftarUmkm->alamat_usaha !!}</p>
                        <p class="text-muted" data-toggle="tooltip" data-placement="bottom" title="{!! $row->produkUmkm->deskripsi_produk !!}">{!! substr($row->produkUmkm->deskripsi_produk,0,50) !!}...</p>
                        @php
                        switch ($row->standart_kemasan) {
                            case 'Sangat Bagus':
                                $type = 'success';
                                break;

                            case 'Bagus':
                                $type = 'info text-white';
                                break;
                            
                            default:
                                $type = 'secondary';
                                break;
                        }
                        @endphp
                        <span class="badge badge-{{ $type }} small">{{ $row->standart_kemasan }}</span>
                    </div>
                </div>
            </div>
        </div>
@endforeach
</div>

@include('components.pagination', ['data' => $data])